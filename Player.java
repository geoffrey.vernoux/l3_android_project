package com.android.ns.gv.tictactoe.model;

import android.view.View;
import java.util.ArrayList;

/** Model Class: Player
 * Classe abstraite "Player" qui est redéfinie dans les différentes classes "filles" : HumanPlayer et ComputerPlayer
 * Contient le symbole (X ou O)
 * Contient les cellules choisies jusqu'à présent par un joueur.
 * Author: NS & GV
 */

public abstract class Player {
    protected boolean active;
    protected ArrayList<Integer> cellsChosen = new ArrayList<>(); // Liste des cellules contenant les symboles du joueur
    protected Board board;
    protected int playerMark;
    public Player(Board board, int playerMark) {
        this.board = board;
        this.playerMark = playerMark;
    }
    public ArrayList<Integer> getCellsChosen() {
        return cellsChosen;
    }
    public void addChosenCell(Integer cellNumber) {
        cellsChosen.add(cellNumber);
    }
    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }
    public abstract void playTurn(View view);
}
