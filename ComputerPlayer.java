package com.android.ns.gv.tictactoe.model;

import android.view.View;
import android.widget.Button;
import java.util.Random;

/**
 * Controller Class: ComputerPlayer
 * Une IA simple qui choisi aléatoirement une cellules pour y placer son symbole
 * Author: NS & GV
 */

public class ComputerPlayer extends Player {
    public ComputerPlayer(Board board, int playerMark) {
        super(board, playerMark);
    }
    Random rand = new Random();

    @Override
    public void playTurn(View view) {
        int int_random = rand.nextInt(board.getEmptyCells().size());
        if (board.getEmptyCells().size() > 1) {
            int buttonID = board.getEmptyCells().get(int_random);
            Button buttonSelected = view.getRootView().findViewById(buttonID);
            buttonSelected.setEnabled(false);
            buttonSelected.setBackgroundResource(playerMark);
            addChosenCell(buttonID);
            board.occupyCell(buttonID);
        }
    }

}
