package com.android.ns.gv.tictactoe.model;

import android.view.View;
import android.widget.Button;

/**
 * Model Class: HumanPlayer
 * Défini le tour d'un joueur humain
 * Author: NS & GV
 */

public class HumanPlayer extends Player {
    public HumanPlayer(Board board, int playerMark) {
        super(board, playerMark);
    }

    @Override
    public void playTurn(View view) {
        Button buttonSelected = (Button) view;
        buttonSelected.setEnabled(false);
        buttonSelected.setBackgroundResource(playerMark);
        addChosenCell(buttonSelected.getId());
        board.occupyCell(buttonSelected.getId());
    }
}
