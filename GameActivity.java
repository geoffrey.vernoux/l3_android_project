package com.android.ns.gv.tictactoe.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.android.ns.gv.tictactoe.R;
import com.android.ns.gv.tictactoe.controller.GameController;
import com.android.ns.gv.tictactoe.model.Board;
import com.android.ns.gv.tictactoe.model.ComputerPlayer;
import com.android.ns.gv.tictactoe.model.HumanPlayer;
import com.android.ns.gv.tictactoe.model.Player;
import java.util.Timer;
import java.util.TimerTask;

/**
 * View Class: GameActivity
 * La vue où le plateau de jeu prend place
 * Author: NS & GV
 */

public class GameActivity extends AppCompatActivity {
    private Player player1;
    private Player player2;
    private boolean isSoloGameMode;
    private GameController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        isSoloGameMode = getIntent().getBooleanExtra(getString(R.string.intent_extra_key_solo_game_mode), true);
        setupActivity();
    }
    public void buClick(View view) {
        if (isSoloGameMode)
            controller.playSoloMode(view);
        else
            controller.playMultiMode(view);
        if (controller.isPlayerWinner(player1))
            notifyResultAndEndGame(R.string.x_won);
        else if (controller.isPlayerWinner(player2))
            notifyResultAndEndGame(R.string.o_won);
        else if (controller.isTieGame())
            notifyResultAndEndGame(R.string.tie_game);
    }
    private void setupActivity() {
        Board board = new Board();
        player1 = new HumanPlayer(board, R.drawable.x);
        player1.setActive(true);
        player2 = isSoloGameMode ? new ComputerPlayer(board, R.drawable.o) : new HumanPlayer(board, R.drawable.o);
        controller = new GameController(board, player1, player2);
    }
    private void notifyResultAndEndGame(int messageRes) {
        Toast.makeText(this, messageRes, Toast.LENGTH_LONG).show();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                finish();
            }
        }, 1500);
    }
}
